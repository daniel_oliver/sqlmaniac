﻿//------------------------------------------------------------------------------
// <copyright file="SqlManiacWindowPackageGuids.cs" company="Company">
//         Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace SqlManiac2
{
    using System;

    /// <summary>
    /// SqlManiacWindow GUID constants.
    /// </summary>
    internal static class SqlManiacWindowPackageGuids
    {
        /// <summary>
        /// SqlManiacWindow GUID string.
        /// </summary>
        public const string PackageGuidString = "80694f41-69eb-4a82-b636-e1bc5ad89e7a";
    }
}
﻿//------------------------------------------------------------------------------
// <copyright file="SqlManiacWindowControl.xaml.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace SqlManiac2
{
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using Microsoft.Data.ConnectionUI;
    using Akka.Actor;



    /// <summary>
    /// Interaction logic for SqlManiacWindowControl.
    /// </summary>
    public partial class SqlManiacWindowControl: System.Windows.Controls.UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlManiacWindowControl"/> class.
        /// </summary>
        public SqlManiacWindowControl()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles click on the button by displaying a message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        [SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions", Justification = "Sample code")]
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Default event handler naming pattern")]
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            using(DataConnectionDialog dcd = new DataConnectionDialog())
            {
                DataSource.AddStandardDataSources(dcd);
                if(DataConnectionDialog.Show(dcd) == DialogResult.OK)
                {
                    var msg = ManiacLogic.SqlConnectionMessage.NewSqlConnectionToTry(dcd.ConnectionString);
                    CentralService.Instance.CentralActor.Tell(msg);
                }
            }
        }
    }
}
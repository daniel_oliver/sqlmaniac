﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;

namespace SqlManiac2
{
    internal class CentralActor: ReceiveActor
    {
        private readonly IActorRef _maniacLogic;
        private readonly CentralService _service;

        public CentralActor(CentralService service)
        {
            _maniacLogic = Context.ActorOf(Props.Create<ManiacLogic.EngineActor>(), "engine");
            _service = service;

            Receive<ManiacLogic.SqlConnectionMessage.SqlConnectionToTry>(msg =>
            {
                _maniacLogic.Forward(msg);
                return true;
            });
            Receive<ManiacLogic.ProcessedStore>(msg => 
            {
                _service.Store = msg;
                return true;
            });
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using ManiacLogic;

namespace SqlManiac2
{
    /// <summary>
    /// This is a global class because there is no main function to serve as the entry point. 
    /// I make myself feel better by calling it a "CentralService".
    /// </summary>
    internal class CentralService
    {
        #region Static
        /// <summary>
        /// Singleton
        /// </summary>
        public static CentralService Instance
        { get; private set; }

        static CentralService()
        {
            Instance = new CentralService();
        }
        #endregion
        #region Instance
        public ActorSystem AkkaActorSystem
        { get { return _system; } }

        public IActorRef CentralActor
        { get { return _central; } }

        private readonly ActorSystem _system;
        private readonly IActorRef _central;

        public ManiacLogic.ProcessedStore Store
        { get; set; }

        private CentralService()
        {
            _system = ActorSystem.Create("SqlManiac-actors");
            Store = ProcessedStore.Empty;
            _central = AkkaActorSystem.ActorOf(Props.Create(() => new CentralActor(this)), "central");
        }
        #endregion
    }
}

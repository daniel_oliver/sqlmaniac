﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Operations;

namespace SqlManiac2
{
    internal class SqlQuickInfoSource: IQuickInfoSource
    {
        public SqlQuickInfoSource(SqlQuickInfoSourceProvider provider, ITextBuffer subjectBuffer)
        {
            m_provider = provider;
            m_subjectBuffer = subjectBuffer;

            //these are the method names and their descriptions
            m_dictionary = new Dictionary<string, string>();
            m_dictionary.Add("add", "int add(int firstInt, int secondInt)\nAdds one integer to another.");
            m_dictionary.Add("subtract", "int subtract(int firstInt, int secondInt)\nSubtracts one integer from another.");
            m_dictionary.Add("multiply", "int multiply(int firstInt, int secondInt)\nMultiplies one integer by another.");
            m_dictionary.Add("divide", "int divide(int firstInt, int secondInt)\nDivides one integer by another.");
        }

        public void AugmentQuickInfoSession(IQuickInfoSession session, IList<object> qiContent, out ITrackingSpan applicableToSpan)
        {
            // Map the trigger point down to our buffer.
            SnapshotPoint? subjectTriggerPoint = session.GetTriggerPoint(m_subjectBuffer.CurrentSnapshot);
            if(!subjectTriggerPoint.HasValue)
            {
                applicableToSpan = null;
                return;
            }

            var store = CentralService.Instance.Store;

            ITextSnapshot currentSnapshot = subjectTriggerPoint.Value.Snapshot;
            SnapshotSpan querySpan = new SnapshotSpan(subjectTriggerPoint.Value, 0);

            //look for occurrences of our QuickInfo words in the span
            ITextStructureNavigator navigator = m_provider.NavigatorService.GetTextStructureNavigator(m_subjectBuffer);
            TextExtent extent = navigator.GetExtentOfWord(subjectTriggerPoint.Value);
            string searchText = extent.Span.GetText();

            if(store.QuickInfo.ContainsKey(searchText))
            {
                var foundVal = store.QuickInfo[searchText];

                applicableToSpan = currentSnapshot.CreateTrackingSpan
                (
                                        //querySpan.Start.Add(foundIndex).Position, 9, SpanTrackingMode.EdgeInclusive
                                        extent.Span.Start, searchText.Length, SpanTrackingMode.EdgeInclusive
                );
                qiContent.Add(foundVal);
            }
            else
            {
                applicableToSpan = null;
            }
            
        }

        public void Dispose()
        {
            if(!m_isDisposed)
            {
                GC.SuppressFinalize(this);
                m_isDisposed = true;
            }
        }

        private Dictionary<string, string> m_dictionary;
        private bool m_isDisposed;
        private SqlQuickInfoSourceProvider m_provider;
        private ITextBuffer m_subjectBuffer;
    }
}
﻿using System.ComponentModel.Composition;
using Microsoft.VisualStudio.Language.Intellisense;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Operations;
using Microsoft.VisualStudio.Utilities;

namespace SqlManiac2
{
    [Export(typeof(IQuickInfoSourceProvider))]
    [Name("ToolTip QuickInfo Source")]
    [Order(Before = "Default Quick Info Presenter")]
    [ContentType("text")]
    internal class SqlQuickInfoSourceProvider: IQuickInfoSourceProvider
    {
        public IQuickInfoSource TryCreateQuickInfoSource(ITextBuffer textBuffer)
        {
            return new SqlQuickInfoSource(this, textBuffer);
        }

        [Import]
        internal ITextStructureNavigatorSelectorService NavigatorService
        { get; set; }

        [Import]
        internal ITextBufferFactoryService TextBufferFactoryService
        { get; set; }
    }
}
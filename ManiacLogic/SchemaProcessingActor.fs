﻿module ManiacLogic.SchemaProcessingActor

open Akka.Actor
open Akka.FSharp
open ManiacLogic

let ColumnToQuickInfo(column : ColumnMessage) = 
  let dataTypeString = 
    match column.DataType with
    | "bigint" | "bit" | "date" | "binary" | "datetime" | "int" | "money" | "smallint" | "smallmoney" | "time" | "timestamp" | "tinyint" -> 
      column.DataType
    | "decimal" | "float" | "numeric" -> 
      sprintf "%s(%i,%i)" column.DataType column.NumericPrecision.Value column.NumericScale.Value
    | "char" | "nchar" | "nvarchar" | "varchar" -> sprintf "%s(%i)" column.DataType column.CharacterMaximumLength.Value
    | _ -> column.DataType
  
  let nullString = 
    (if column.IsNullable then "   NULL"
     else "")
  
  let defaultString = 
    (if column.ColumnDefault.Replace("(", "").Replace(")", "").Length > 0 then 
       "   DEFAULT(" + column.ColumnDefault + ")"
     else "")
  
  //Optional strings have their spacing prepended
  //TableSchema, TableName, ColumnName, DataType, Optional(Nullable), Optional(DefaultValue)
  sprintf "%s.%s.%s   %s%s%s" column.TableSchema column.TableName column.ColumnName dataTypeString nullString 
    defaultString

let ProcessColumns(columns : ColumnMessage list) = 
  columns
  |> Seq.groupBy (fun col -> col.ColumnName)
  |> Seq.map 
       (fun (colName, columns) -> 
       colName, System.String.Join(System.Environment.NewLine, (columns |> Seq.map ColumnToQuickInfo)))

let HandleMessage (mailbox : Actor<SqlConnectionMessage>) msg = 
  //Do some post processing on the schema
  match msg with
  | SqlConnectionSuccess(connString, raw) -> 
    mailbox.Context.Parent <! { ConnectionString = connString
                                QuickInfo = 
                                  raw.Columns
                                  |> ProcessColumns
                                  |> Map.ofSeq }
  | _ -> ()
  ()

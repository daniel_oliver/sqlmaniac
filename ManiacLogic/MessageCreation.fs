﻿module ManiacLogic.MessageCreation

open ManiacLogic
open SqlHelper
open System
open System.Data

let DataRowToColumnMessage(row : DataRow) = 
  { TableCatalog = Convert.ToString row.["table_catalog"]
    TableSchema = Convert.ToString row.["table_schema"]
    TableName = Convert.ToString row.["table_name"]
    ColumnName = Convert.ToString row.["column_name"]
    OrdinalPosition = Convert.ToInt32 row.["ordinal_position"]
    ColumnDefault = Convert.ToString row.["column_default"]
    IsNullable = row.["is_nullable"].ToString().Equals("YES", StringComparison.InvariantCultureIgnoreCase)
    DataType = Convert.ToString row.["data_type"]
    CharacterMaximumLength = OptionInt32 row.["character_maximum_length"]
    CharacterOctetLength = OptionInt32 row.["character_octet_length"]
    NumericPrecision = OptionInt32 row.["numeric_precision"]
    NumericPrecisionRadix = OptionInt32 row.["numeric_precision_radix"]
    NumericScale = OptionInt32 row.["numeric_scale"]
    DatetimePrecision = OptionInt32 row.["datetime_precision"]
    CharacterSetCatalog = OptionString row.["character_set_catalog"]
    CharacterSetName = OptionString row.["character_set_name"]
    CollationCatalog = OptionString row.["collation_catalog"] }

let DataRowToTableMessage(row : DataRow) = 
  { TableCatalog = Convert.ToString row.["table_catalog"]
    TableSchema = Convert.ToString row.["table_schema"]
    TableName = Convert.ToString row.["table_name"]
    TableType = Convert.ToString row.["table_type"] }

﻿namespace ManiacLogic

type ProcessedStore = 
  { ConnectionString : string
    QuickInfo : Map<string, string> }
  static member Empty = 
    { ConnectionString = ""
      QuickInfo = Map.empty }

﻿namespace ManiacLogic

open Akka.Actor
open Akka.FSharp
open ManiacLogic

type EngineActor() as this = 
  inherit ReceiveActor()
  let context = ReceiveActor.Context
  let postProcessActors = spawn context "processor" (actorOf2 SchemaProcessingActor.HandleMessage)
  let sqlConnActors = spawn context "connection" (actorOf2 (SqlConnectionActor.HandleMessage postProcessActors))
  
  let SqlConMsg(msg : SqlConnectionMessage) = 
    match msg with
    | SqlConnectionToTry sqlConnStr -> sqlConnActors <! msg
    | _ -> context.Parent <! msg
    true
  
  let PostProcessMsg(msg : ProcessedStore) = 
    context.Parent <! msg
    true
  
  do 
    this.Receive(SqlConMsg)
    this.Receive(PostProcessMsg)

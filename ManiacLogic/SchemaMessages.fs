﻿namespace ManiacLogic

//Drawn from  https://msdn.microsoft.com/library/ms254969.aspx
type ColumnMessage = 
  { TableCatalog : string
    TableSchema : string
    TableName : string
    ColumnName : string
    OrdinalPosition : int
    ColumnDefault : string
    IsNullable : bool
    DataType : string
    CharacterMaximumLength : int option
    CharacterOctetLength : int option
    NumericPrecision : int option
    NumericPrecisionRadix : int option
    NumericScale : int option
    DatetimePrecision : int option
    CharacterSetCatalog : string option
    CharacterSetName : string option
    CollationCatalog : string option }

type TableMessage = 
  { TableCatalog : string
    TableSchema : string
    TableName : string
    TableType : string }

type SchemaMessages = 
  | Column of ColumnMessage
  | Table of TableMessage

///Before post processing
type RawSchema = 
  { Tables : TableMessage list
    Columns : ColumnMessage list }

///The connection string
type SqlConnectionMessage = 
  | SqlConnectionToTry of string
  | SqlConnectionSuccess of string * RawSchema
  | SqlConnectionFailure of string

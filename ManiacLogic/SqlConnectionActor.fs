﻿module ManiacLogic.SqlConnectionActor

open Akka.Actor
open Akka.FSharp
open ManiacLogic
open System.Data
open System.Data.SqlClient

//http://getakka.net/docs/FSharp%20API
///The msg should be a connection string
let HandleMessage (processors : IActorRef) (mailbox : Actor<_>) msg = 
  match msg with
  | SqlConnectionToTry connString -> 
    try 
      use sqlConn = new SqlConnection(connString)
      sqlConn.Open()
      //Get schema info
      let columnTable = sqlConn.GetSchema("Columns")
      let tableTable = sqlConn.GetSchema("Tables")
      
      let answer = 
        { Columns = 
            [ for row in columnTable.Rows -> MessageCreation.DataRowToColumnMessage row ]
          Tables = 
            [ for row in tableTable.Rows -> MessageCreation.DataRowToTableMessage row ] }
      
      let result = SqlConnectionSuccess(connString, answer)
      mailbox.Context.Sender <! result
      processors <! result
      ()
    with _ -> 
      mailbox.Context.Sender <! SqlConnectionFailure connString //Catching an exception may not be the best actor policy
      ()
  | _ -> () //Ignore other messages

﻿namespace ManiacLogic

///A very large lookup table
///If anything has massive performance hits, this is it.
type StructuredSchema(raw : RawSchema) = 
  let matchColumnWithTable (column : ColumnMessage) (table : TableMessage) = 
    table.TableName = column.TableName && table.TableSchema = column.TableSchema 
    && table.TableCatalog = column.TableCatalog
  
  /// Column * Table
  member this.ColumnByTable = 
    raw.Columns
    |> List.map (fun col -> col, (raw.Tables |> List.find (matchColumnWithTable col)))
    |> Map.ofList
  
  /// Table * Column list
  ///This could probably be made more efficient by leveraging the already existing ColumnByTable
  member this.TableByColumns = 
    raw.Columns
    |> List.groupBy (fun col -> (raw.Tables |> List.find (matchColumnWithTable col)))
    |> Map.ofList

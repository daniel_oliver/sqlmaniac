﻿module ManiacLogic.SqlHelper

open System

type ConvertFromObject<'a> = Object -> 'a

let ConvertNullable (convFunc : ConvertFromObject<_>) value =
  if DBNull.Value.Equals(value) then None
  else Some(convFunc value)

let OptionString<'a> = ConvertNullable Convert.ToString
let OptionInt32<'a> = ConvertNullable Convert.ToInt32